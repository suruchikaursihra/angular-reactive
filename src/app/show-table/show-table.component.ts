import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServlogService } from '../servlog.service';


@Component({
  selector: 'app-show-table',
  templateUrl: './show-table.component.html',
  styleUrls: ['./show-table.component.css']
})
export class ShowTableComponent implements OnInit {

  logdate= new Date(2018,7,2);
  value: any;
  constructor(private servicelog : ServlogService, private route: Router) {
    
    this.value=this.servicelog.getData();
  }


  ngOnInit() {
  }

  redirect() {
    this.route.navigate(['/main']);
  }

}
