import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { RouterModule, Routes, Router } from '@angular/router';
import { THROW_IF_NOT_FOUND } from '../../../node_modules/@angular/core/src/di/injector';
import { ServlogService } from '../servlog.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  title = 'reactive-forms';
  login: FormGroup;
  checkpass:Boolean;
  pass:string="password";
  pass1:string="password";

  constructor(private servicelog : ServlogService ,private route: Router) {
    this.login = new FormGroup({
      Fname: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
      Lname: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
      email: new FormControl('', [Validators.required,  Validators.email]),
      contact: new FormControl('', [Validators.required, Validators.pattern("^(([0-9]*)|(([0-9]*)))$")]),
      password: new FormControl('', [Validators.required]),
      cnfpassword: new FormControl('', [Validators.required]),
      eid: new FormControl('', [Validators.required, Validators.pattern("^(([0-9]*)|(([0-9]*)))$")]),
      gender: new FormControl('', [Validators.required, Validators.pattern("^male$|^female$")])
    });
  }


  ngOnInit() {
     if(this.route.url==='/main')
 {   let person =this.servicelog.getData();
  if(person!=null)
     this.login.patchValue({
       Fname: person.Fname,
       Lname: person.Lname,
       contact: person.contact,
       gender: person.gender,
       eid: person.eid,
       email: person.email,
       password: person.password,
       cnfpassword: person.cnfpassword
     });
   }
  
}
  
  check(){
    if(this.login.value.password!==this.login.value.cnfpassword)
    this.checkpass=false;
    else
    this.checkpass=true;
    console.log(this.checkpass);
    }

    showpass(){
      if(this.pass=="text")
      this.pass='password';
      else{
        this.pass="text"; 
      }
    }
      showpass1(){
        if(this.pass1=="password")
        this.pass1='text';
        else{
          this.pass1="password"; 
        }
    }

    onclick(){
      this.servicelog.setData(this.login.value);
      this.route.navigate(['/table']);
    }
}

