import { TestBed, inject } from '@angular/core/testing';

import { ServlogService } from './servlog.service';

describe('ServlogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServlogService]
    });
  });

  it('should be created', inject([ServlogService], (service: ServlogService) => {
    expect(service).toBeTruthy();
  }));
});
