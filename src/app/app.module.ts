import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormControl } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes,Router } from '@angular/router';
import { ShowTableComponent } from './show-table/show-table.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ServlogService } from './servlog.service';
import { LogpipPipe } from './logpip.pipe';


const route: Routes = [
  { path: 'table', 
    component: ShowTableComponent },
  {
    path: 'main',
    component: LoginFormComponent
  },
  {
    path: '',
    component: LoginFormComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ShowTableComponent,
    LoginFormComponent,
    LogpipPipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(route)

  ],
  providers: [ ServlogService ],
  bootstrap: [AppComponent]
})
export class AppModule { 

  Fname :FormControl= new FormControl('');
}
